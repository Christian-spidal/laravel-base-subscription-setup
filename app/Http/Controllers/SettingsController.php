<?php

namespace App\Http\Controllers;

use App\settings;
use Auth;
use Stripe;
use Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$subscriptions = Auth::user()->subscriptions;
        $user = Auth::user();
        $invoices = $user->invoices();
        $plan = $user->retrieve_my_plan();
        $permissions = $user->retrieve_my_permissions();
        return view('user/settings', compact('user','invoices','plan','permissions'));
    }

    public function my_account()
    {
        //
        //$subscriptions = Auth::user()->subscriptions;
        
        return view('user/my_account', compact('user'));
    }

    public function my_subscription()
    {
        //
        $user = Auth::user();
        $plan = $user->retrieve_my_plan();
        return view('user/my_subscription', compact('plan'));
    }

    public function my_settings()
    {
        //
        //$subscriptions = Auth::user()->subscriptions;
        
        return view('user/my_settings', compact('user','invoices','plan','permissions'));
    }

    public function my_invoices()
    {
        //
        //$subscriptions = Auth::user()->subscriptions;
        $user = Auth::user();
        $invoices = $user->invoices();
        return view('user/my_invoices', compact('invoices'));
    }

    public function change_password() {
        
        return view('user/reset_password');
    }

    public function update_password() {

        $user = Auth::user();

        //if($user->Password === bcrypt(Request('old_password'))) {
        if(Hash::check( Request('old_password'), $user->password )) {

            // auth is correct you can change the users password
            if(Request('new_password') == Request('password_confirmation')) {
                $user->Password = bcrypt(Request('new_password'));
                $user->save();
                $message = 'Your password has been updated.';
            } else {
                $message = 'Your passwords do not match.';
            }
        } else {
            // auth is incorrect show error msg
            $message = 'Your current password is incorrect.';
        }
        return redirect('/user/settings/change_password')->with('message', $message);
    }

    public function change_plan() {

        $user = Auth::user();

        $plan = $user->retrieve_my_plan();

        $user->subscription( $plan[0]->nickname )->swap( Request('plan_id') , Request('plan_name') );

        $message = "Your subscription has been changed to " . Request('plan_name');

        return redirect('/user/settings/my_subscription')->with('message', $message);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function show(settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit(settings $settings)
    {
        //
        $id = Auth::user();
        $user = User::find($id);
        $user->name = Request('name');
        $user->email = Request('email');
        $user->save();
        return view('AccountSettings/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //$data = Request::all();
        //dd(Request('name'));

        $user = Auth::user();
        $user->Name = Request('name_field');
        $user->Email = Request('email_field');

        $user->save();

        return redirect('/user/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(settings $settings)
    {
        //
    }
}
