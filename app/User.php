<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Stripe;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function retrieve_my_plan() {
        // Get all stripe plans
        \Stripe\Stripe::setApiKey("sk_test_ybYNqOI3JEavfBSpdkqP0fOA");
        $plans = \Stripe\Plan::all();
        $my_plans = array();
        foreach($plans['data'] as $plan) {
            if (Auth::user()->subscribed($plan['nickname'])) {
                $my_plans[] = $plan;
            }
        }
        return $my_plans;
    }

    public function retrieve_my_permissions() {
         // Get all stripe plans
        \Stripe\Stripe::setApiKey("sk_test_ybYNqOI3JEavfBSpdkqP0fOA");
        // Get all plans from stripe
        $plans = \Stripe\Plan::all();
        // create an array to store users plan data in
        $my_plans = array();
        // loop through all the stripe plans
        foreach($plans['data'] as $plan) {
            // check to see if the user is subscribed to this plan.
            if (Auth::user()->subscribed($plan['nickname'])) {
                // get users permissions based on their plan
                $my_plans_permissions[] = $this->plan_permissions($plan['nickname']);
            }
        }
        // return plan permissions
        return $my_plans_permissions;
    }

    public function plan_permissions($plan_name) {
        // Check Plan
        if($plan_name == "Bronze") {
            // Setu[ array tp store permissions
            $permissions = array();
            $permissions['maximum_campaigns'] = "1";
            $permissions['teams'] = false;
            return $permissions;
        } else if($plan_name == "Silver") {
            // Setu[ array tp store permissions
            $permissions = array();
            $permissions['maximum_campaigns'] = "5";
            $permissions['teams'] = true;
            return $permissions;
        }
    }

}
