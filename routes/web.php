<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
	//\Stripe\Stripe::setApiKey(config('services.stripe.secret'));
	//$plans = \Stripe\Plan::all();
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/settings', 'SettingsController@index');
Route::get('/user/settings/my_account', 'SettingsController@my_account');
Route::get('/user/settings/my_subscription', 'SettingsController@my_subscription');
Route::get('/user/settings/my_settings', 'SettingsController@my_settings');
Route::get('/user/settings/my_invoices', 'SettingsController@my_invoices');
Route::get('/user/settings/change_password','SettingsController@change_password');

Route::post('/user/settings/update','SettingsController@update');
Route::post('/user/settings/update_password','SettingsController@update_password');
Route::post('/user/settings/change_plan','SettingsController@change_plan');