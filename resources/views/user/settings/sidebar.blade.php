<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/user/settings/my_account">My Account</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/user/settings/change_password">Reset Password</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/user/settings/my_subscription">My Subscription</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/user/settings/my_invoices">My Invoices</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/user/settings/my_settings">Privacy Settings</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/') }}/logout">Log Out</a>
  </li>
</ul>