@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                   
                   @include('user/settings/sidebar')

                </div>
            </div>    
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif 

                    <p>You are currently subscribed to the <a href="">{{ $plan[0]->nickname }}</a> plan paying £{{ number_format($plan[0]->amount / 100, 2) }} per {{ $plan[0]->interval }}.</p>

                    <?php var_dump($permissions); ?>

                    <table>
                        @foreach ($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                <td>{{ $invoice->total() }}</td>
                                <td><a href="/user/invoice/{{ $invoice->id }}">Download</a></td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>    
        </div>
    </div>
</div>
@endsection
