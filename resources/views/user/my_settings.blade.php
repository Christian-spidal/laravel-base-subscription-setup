@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                   
                   @include('user/settings/sidebar')

                </div>
            </div>    
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">My Settings</div>
               
                <div class="card-body">
                       
                </div>
            </div>    
        </div>
    </div>
</div>
@endsection
