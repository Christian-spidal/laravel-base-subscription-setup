@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                   
                   @include('user/settings/sidebar')

                </div>
            </div>    
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">Invoices</div>
               
                <div class="card-body">
                 
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th>Date invoiced</th>
                                <th>Amount invoiced</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                    <td>{{ $invoice->total() }}</td>
                                    <td><a href="/user/invoice/{{ $invoice->id }}">Download</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>    
        </div>
    </div>
</div>
@endsection
