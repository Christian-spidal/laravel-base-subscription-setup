@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                   
                   @include('user/settings/sidebar')

                </div>
            </div>    
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">My Subscription</div>
               
                <div class="card-body">
                    <p>You are currently subscribed to the <a href="">{{ $plan[0]->nickname }}</a> plan paying £{{ number_format($plan[0]->amount / 100, 2) }} per {{ $plan[0]->interval }}.</p>   
                </div>
            </div>
            <br />
            <div class="card-deck mb-3 text-center">
                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Bronze</h4>
                  </div>
                  <div class="card-body">
                    <h1 class="card-title pricing-card-title">£20 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>10 users included</li>
                      <li>2 GB of storage</li>
                      <li>Email support</li>
                      <li>Help center access</li>
                    </ul>
                    <form action="{{ url('/') }}/user/settings/change_plan" method="POST">
                      @csrf
                      <input type="hidden" name="plan_id" value="Plan 1">
                      <input type="hidden" name="plan_name" value="Bronze">
                      @if( $plan[0]->nickname == "Bronze" )
                        <button type="submit" class="btn btn-lg btn-block btn-primary" disabled>Current</button>
                      @else
                        <button type="submit" class="btn btn-lg btn-block btn-primary">Downgrade</button>
                      @endif
                    </form>
                  </div>
                </div>
                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Silver</h4>
                  </div>
                  <div class="card-body">
                    <h1 class="card-title pricing-card-title">£40 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>20 users included</li>
                      <li>10 GB of storage</li>
                      <li>Priority email support</li>
                      <li>Help center access</li>
                    </ul>
                    <form action="{{ url('/') }}/user/settings/change_plan" method="POST">
                      @csrf
                      <input type="hidden" name="plan_id" value="Plan 2">
                      <input type="hidden" name="plan_name" value="Silver">
                      @if( $plan[0]->nickname == "Silver" )
                        <button type="submit" class="btn btn-lg btn-block btn-primary" disabled>Current</button>
                      @else
                        @if( $plan[0]->nickname == "Bronze" )
                          <button type="submit" class="btn btn-lg btn-block btn-primary">Upgrade</button>
                        @else
                          <button type="submit" class="btn btn-lg btn-block btn-primary">Downgrade</button>
                        @endif
                      @endif
                    </form>
                  </div>
                </div>
                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Gold</h4>
                  </div>
                  <div class="card-body">
                    <h1 class="card-title pricing-card-title">£80 <small class="text-muted">/ mo</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>30 users included</li>
                      <li>15 GB of storage</li>
                      <li>Phone and email support</li>
                      <li>Help center access</li>
                    </ul>
                    <form action="{{ url('/') }}/user/settings/change_plan" method="POST">
                      @csrf
                      <input type="hidden" name="plan_id" value="Plan 3">
                      <input type="hidden" name="plan_name" value="Gold">
                      @if( $plan[0]->nickname == "Gold" )
                        <button type="submit" class="btn btn-lg btn-block btn-primary" disabled>Current</button>
                      @else
                        @if( $plan[0]->nickname == "Silver" || $plan[0]->nickname == "Bronze" )
                          <button type="submit" class="btn btn-lg btn-block btn-primary">Upgrade</button>
                        @else
                          <button type="submit" class="btn btn-lg btn-block btn-primary">Downgrade</button>
                        @endif
                      @endif
                    </form>
                  </div>
                </div>
              </div>

        </div>
    </div>
</div>
@endsection
