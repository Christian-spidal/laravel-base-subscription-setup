@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <div class="card">
                <div class="card-header">Your Settings</div>
               
                <div class="card-body">
                   
                   @include('user/settings/sidebar')

                </div>
            </div>    
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">Your Account</div>
               
                <div class="card-body">
                    <form method="POST" action="/user/settings/update">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="formGroupExampleInput">Name</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="name_field" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" name="email_field" aria-describedby="emailHelp" value="{{ Auth::user()->email }}">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">First line of address</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Second line of address</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">County</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Postcode</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Country</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" value="{{ Auth::user()->name }}">
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>   
                </div>
            </div>    
        </div>
    </div>
</div>
@endsection
